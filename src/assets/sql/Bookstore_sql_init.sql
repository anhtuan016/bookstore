drop table if exists "public".t_sale_detail;
drop table if exists "public".t_sale;
drop table if exists "public".t_order_detail;
drop table if exists "public".t_order;
drop table if exists "public".m_code;
drop table if exists "public".m_product_category;
drop table if exists "public".m_product;
drop table if exists "public".m_category;
drop table if exists "public".m_user_detail;
drop table if exists "public".m_token;
drop table if exists "public".m_user;

drop sequence if exists "public".m_product_sequence;
drop sequence if exists "public".m_category_sequence;
drop sequence if exists "public".m_user_sequence;
drop sequence if exists "public".m_token_sequence;

create sequence "public".m_product_sequence start 1;
create sequence "public".m_category_sequence start 1;
create sequence "public".m_user_sequence start 1;
create sequence "public".m_token_sequence start 1;

create table "public".m_code (
  code_id integer not null
  , parent_cd integer not null
  , cd character varying(30) not null
  , name character varying(50) not null
  , display_order integer
  , remark character varying(1000) not null
  , del_flg boolean default false
  , create_datetime timestamp(6) without time zone
  , create_id character varying(64)
  , update_datetime timestamp(6) without time zone
  , update_id character varying(64)
  , primary key (code_id)
);
COMMENT ON COLUMN m_code.code_id IS 'cd = parent_cd * 1000 + count number';

create table "public".m_category(
	id integer primary key default nextval('m_category_sequence')
	, name varchar(100) not null
	, status integer
  	, del_flg boolean default false
  	, create_datetime timestamp(6) without time zone
  	, create_id character varying(64)
  	, update_datetime timestamp(6) without time zone
  	, update_id character varying(64)
);
ALTER SEQUENCE m_category_sequence OWNED BY m_category.id;

create table "public".m_product (
  id integer not null default nextval('m_product_sequence')
  , name character varying(250) not null
  , type character varying(50) not null
  , "size" integer default 0
  , provider varchar (100),
  , link_1 varchar (100)
  , link_2 varchar (100)
  , link_3 varchar (100)
  , link_4 varchar (100)
  , link_5 varchar (100)
  , page integer default 0
  , display_order integer
  , requirement TEXT
  , image varchar(400)
  , status integer
  , del_flg boolean default false
  , create_datetime timestamp(6) without time zone
  , create_id character varying(64)
  , update_datetime timestamp(6) without time zone
  , update_id character varying(64)
  , primary key (id)
);
ALTER SEQUENCE m_product_sequence OWNED BY m_product.id;

create table "public".m_product_category(
	product_id integer references m_product (id)
	, category_id integer references m_category(id)
	, del_flg boolean default false
  	, create_datetime timestamp(6) without time zone
  	, create_id character varying(64)
  	, update_datetime timestamp(6) without time zone
  	, update_id character varying(64)
	, primary key (product_id, category_id)
);

create table "public".m_user(
	id integer primary key default nextval('m_user_sequence')
	, username varchar(50) constraint unique_username unique not null
	, password varchar(256) not null
	, del_flg boolean default false
  	, create_datetime timestamp(6) without time zone
  	, create_id character varying(64)
  	, update_datetime timestamp(6) without time zone
  	, update_id character varying(64)
);
ALTER SEQUENCE m_user_sequence OWNED BY m_user.id;
COMMENT ON TABLE m_user IS 'Table store login information';

create table "public".m_user_detail(
	user_id integer references m_user(id)
	, fullname varchar(256)
	, address_1 varchar (300)
	, address_2 varchar (300)
	, telephone varchar(20)
	, mobile varchar(20)
	, email varchar(150)
	, del_flg boolean default false
  	, create_datetime timestamp(6) without time zone
  	, create_id character varying(64)
  	, update_datetime timestamp(6) without time zone
  	, update_id character varying(64)
	, primary key (user_id)
);
COMMENT ON TABLE m_user_detail IS 'Table store detail user information';


create table "public".m_token(
	id integer primary key default nextval('m_token_sequence')
	, user_id integer references m_user(id)
	, token varchar(260) not null constraint unique_token unique
	, refresh_token varchar(260) not null constraint unique_refresh_token unique
	, expire_time bigint default 1800
	, expired boolean default false
	, del_flg boolean default false
  	, create_datetime timestamp(6) without time zone
  	, create_id character varying(64)
  	, update_datetime timestamp(6) without time zone
  	, update_id character varying(64)
);
ALTER SEQUENCE m_token_sequence OWNED BY m_token.id;
COMMENT ON COLUMN m_token.expire_time IS 'count by second unit - default 30 mins';

create table "public".t_order(
	user_id integer references m_user(id)
	, order_no integer
	, status integer
	, sale boolean
	, total_price bigint
	, order_date varchar(12)
	, order_time varchar(6)
	, del_flg boolean default false
  	, create_datetime timestamp(6) without time zone
  	, create_id character varying(64)
  	, update_datetime timestamp(6) without time zone
  	, update_id character varying(64)
	, primary key(user_id, order_no)
);

create table "public".t_order_detail(
	order_no integer
	, order_count integer
	, product_id integer references m_product(id)
	, amount integer default 0
	, status integer
	, price bigint default 0
	, del_flg boolean default false
  	, create_datetime timestamp(6) without time zone
  	, create_id character varying(64)
  	, update_datetime timestamp(6) without time zone
  	, update_id character varying(64)
	, primary key(order_count, order_no)
);

create table "public".t_sale(
	id SERIAL primary key,
	sale_date varchar(12),
	sale_time varchar(6),
	total_amount bigint,
	discount_amount bigint,
	order_no integer,
	user_id integer,
	del_flg boolean default false,
  	create_datetime timestamp(6) without time zone,
  	create_id character varying(64),
  	update_datetime timestamp(6) without time zone,
  	update_id character varying(64),
	foreign key (user_id, order_no) references t_order(user_id, order_no)
);

create table "public".t_sale_detail(
	sale_id integer references t_sale(id),
	sale_no integer,
	amount bigint,
	discount_amount bigint,
	payment_method integer,
	del_flg boolean default false,
  	create_datetime timestamp(6) without time zone,
  	create_id character varying(64),
  	update_datetime timestamp(6) without time zone,
  	update_id character varying(64),
	primary key (sale_id, sale_no)
);
