import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  @ViewChild('#categorySlide', { static: true }) categorySlide;
  images = [
    'assets/slide1.jpg',
    'assets/slide2.jpg',
    'assets/slide3.jpg',
    'assets/slide4.jpg',
    'assets/slide5.jpg'
  ];
  categoryList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  listBook = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

  constructor() { }

  ngOnInit(): void {
    console.log(this.categorySlide);
  }

}
