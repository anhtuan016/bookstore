import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.scss']
})
export class BookDetailComponent implements OnInit {
  @Input() imgUrl: string;
  @Input() notFoundImg = '';
  @Input() title: string;
  @Input() price: string;
  @Input() detail: string;
  constructor() { }

  ngOnInit(): void {
  }

}
