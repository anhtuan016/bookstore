import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  sideBarToggler = {active : false};
  constructor() { }

  ngOnInit(): void {
  }
  toggle(){
    this.sideBarToggler.active = !this.sideBarToggler.active;
  }
}
